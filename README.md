# Throttle-debug

Tool to do graphical comparison of the shift in framerate of the [cam-express](https://gitlab.cern.ch/mro/common/www/cam-express) project cameras between the theoretical and real value

![diagram](docs/diagram.svg)

*explanation diagram with cli:1125ms and api:500ms*

![screenshot](docs/screenshot.png)

*screenshot of computations with cli:1125ms and api:500ms*